var app = angular.module('StarterApp', ['ngMaterial']);

app.controller('AppCtrl', function($scope) {
  $scope.channelPairs = [{
    name: "Channel pair 1",
    color: "green",
    lambda: 1230,
    disabled: false
  }, {
    name: "Channel pair 2",
    color: "red",
    lambda: 760,
    disabled: false
  }];
  $scope.allDisabled = false;
  
  $scope.dummy = 'dummy';

  $scope.simulate = function() {
    $scope.allDisabled = !$scope.allDisabled;
   console.log('test'); $scope.channelPairs.forEach(function(cp) {
       cp.disabled = !cp.disabled;

    });
  }
  
  $scope.labels = ["1" , "2", "3", "4", "5"];
  $scope.series = ['Bytes in', 'Bytes out'];
  $scope.data = [
    [65, 59, 80, 81, 56],
    [28, 48, 40, 19, 86]
  ];

});


jQuery(document).ready(function($){
	var $timeline_block = $('.cd-timeline-block');

	//hide timeline blocks which are outside the viewport
	$timeline_block.each(function(){
		if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
			$(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		}
	});

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		$timeline_block.each(function(){
      var windowDiff = $(window).height() - ($(window).height() * 0.75);
      var fadeOffset = $(this).offset().top + $(this).height();
      var scrollPos = $(window).scrollTop();
      
			if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				$(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
			}
      else if (fadeOffset < (scrollPos + windowDiff)) {
        console.log("Fade Offset: " + fadeOffset);
        console.log("Fade Position: " + (scrollPos + windowDiff))
        $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('bounce-in').addClass('is-hidden');
      }
      /*else {
        console.log("=====================")
        console.log("BlockOne: " + $(this));
        console.log("Top Offset: " + $(this).offset().top);
        console.log("Scroll Top: " + $(window).scrollTop());
        console.log("Window Fade Height: " + $(window).height()*0.75);
        console.log("Element Height: " + $(this).height());
      }*/
		});
	});
});


//------------------------------------------------------
